class ShortUrlsController < ApplicationController
  def index
    @short_url = ShortUrl.new
  end

  def visit
    @short_url = ShortUrl.where(short_code: params[:short_code])
                    .where("created_at > ?",Date.today-1.month).first
    if @short_url.present?
      @visit = @short_url.visits.build
      ## Got these using Geocoder Gem ========
      @visit.visited_ip = request.remote_ip
      @visit.country = request.location.country || 'India'
      @visit.save
      #=========================================      
      @short_url.increment_visit
      redirect_to @short_url.original_url
    else
      redirect_to '/404'
    end
  end

  def stats
    @short_urls = ShortUrl.includes(:visits).order("created_at DESC")
  end
  
  def create
    @short_url = ShortUrl.new(short_url_params)
    respond_to do |format|
      if @short_url.save
        # format.json { render :show, status: :created, location: @short_url }
        format.js {@short_full_url = request.base_url+'/visit/'+@short_url.short_code}
      else
        format.js {}
        # format.json { render json: @short_url.errors, status: :unprocessable_entity }
      end
    end
  end

  
  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def short_url_params
      params.require(:short_url).permit(:original_url, :short_code, :sanitize_url, :view_count)
    end
end
