class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
      t.string :visited_ip
      t.string :country
      t.integer :short_url_id

      t.timestamps null: false
    end
  end
end
