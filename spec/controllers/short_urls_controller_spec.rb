require 'rails_helper'

RSpec.describe ShortUrlsController, type: :controller do

    describe "POST #index" do
        context "with valid attributes" do
            it "create new short code" do
                post :create, {short_url: { :original_url => "https://google.com" },:format => :js}
                expect(ShortUrl.count).to eq(1)
            end
        end

        context "without valid attributes" do
            it "create new short code" do
                post :create, {short_url: { :original_url => "" },:format => :js}
                expect(ShortUrl.count).to eq(0)
            end
        end
    end

    describe "Get #stats" do
        context "with valid attributes" do
            it "Visits stats page" do
                post :stats
                expect(response.status).to eq(200)
            end
        end
    end

    describe "Get #Visit wrong page" do
        context "Visit with random code" do
            it "redirects to 404 page" do
                get :visit,{short_code: '12121'}
                expect(response).to redirect_to('/404')
            end
        end
    end
end